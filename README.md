# Project's README
The objective of this project is to introduce our company, WeChat, to IT students in the University of Sydney.   
Give them a thorough understanding of what graduate positions WeChat is offering for their relevant majors : Computer scienc, 
Software Engineering, Information System and Data Science.  
And, describe the career live and career path as a member in WeChat.

In order to achieve the objective in the Career booth, two tasks need to be completed:  
	1. Prepare a short Presentation (5 min)  
		●	Every person in the group must talk.  
		●	Talk professionally presented (with appropriate dress, being well prepared and being able to answer questions).  
		●	Talk about all the roles we offer, or highlight one role we think may attract students.   
		●	The information presented does not have to be in the Career Fair Booklet.  
		●	May include additional media such as slides, poster etc. but this is not required.  
		●	Print out a copy of your booklet.  
	2. A Career Fair Booklet  
		●	An overview of company describing why it is a desirable employer for University of Sydney IT graduates.  
		●	A description of the types of roles that are available in the company for University of Sydney IT graduates  
		(this must cover all four majors in the  Bachelor of Advanced Computing, an indicate an understanding of their differences, and where they complement each other)  
		●	The skills needed to be successful in the roles (with a focus on the knowledge and skills developed in the Bachelor of Advanced Computing majors)  
		●	A description of the work environment, and career progression that graduates can expect at the company.  

 Online Group Meeting 12.May.2018:  
 Firstly, all team members agreed on focusing on making the Career Booklet first. Only upon completing the booklet, we then prepare the presentation based on the content of the booklet.  
 The following tasks are distributed among the group members:  
 	YuYang: Source for images that can be used in the project booklet. The images can be related to WeChat working environment and employee welfare.  
	Ceci: Design the structure for the Booklet  
	Qiangsheng: Translate the structure designed by Ceci to Pdf by using LaTex, fill in any content wherever possible base on existing research.  
	
 Online Group Meeting 16.May.2018:  
 Progress - We have already gotten the basic structure of the booklet and some source images can be used in the booklet. Also, a scaffold has been created in LaTex 
 which contains some brief instructions of what to do for each each sections.  
	Discussion:  
	- We had some minor dispute over the design of the background, however we decided to focus on the content of the booklet first.  
	- Ceci is going to focus on Section 1 About Us and Section 3 Culture and Welfare.  
	- YuYang will be doing Computer Science and Software engineering as part of Section 2.1 Tech roles, and section 2.2 Project roles and 2.3 Marketing Roles  
	- Qiangsheng will be doing Information System and Data Science as part of Section 2.1, and also the Starting page of Section 2.  
	
	Schedule:
	- All members has to finish the above task before Monday, Week 11.  
	- Examine and cross-modify each other's work to create a complete prototype of the booklet before Wednesday, Week11.  
	- Focus on preparing for the Booth Presentation until before Friday, Week 12.  
	
Directories:  
First draft -include research done by each member.  
Booklet -The LaTex prototype of the booklet.  
